const themePrefix = "ace/theme/";
const languagePrefix = "ace/mode/";
const themes = [
	{ "value" : "ambiance", "textContent" : "Ambiance" }
	,{ "value" : "chaos", "textContent" : "Chaos" }
	,{ "value" : "chrome", "textContent" : "Chrome" }
	,{ "value" : "clouds", "textContent" : "Clouds" }
	,{ "value" : "clouds_midnight", "textContent" : "Clouds Midnight" }
	,{ "value" : "cobalt", "textContent" : "Cobalt" }
	,{ "value" : "crimson_editor", "textContent" : "Crimson Editor" }
	,{ "value" : "dawn", "textContent" : "Dawn" }
	,{ "value" : "dracula", "textContent" : "Dracula" }
	,{ "value" : "dreamweaver", "textContent" : "Dreamweaver" }
	,{ "value" : "eclipse", "textContent" : "Eclipse" }
	,{ "value" : "github", "textContent" : "Github" }
	,{ "value" : "gob", "textContent" : "Gob" }
	,{ "value" : "gruvbox", "textContent" : "Gruvbox" }
	,{ "value" : "idle_fingers", "textContent" : "idle Fingers" }
	,{ "value" : "iplastic", "textContent" : "IPlastic" }
	,{ "value" : "katzenmilch", "textContent" : "KatzenMilch" }
	,{ "value" : "kr_theme", "textContent" : "krTheme" }
	,{ "value" : "kuroir", "textContent" : "Kuroir" }
	,{ "value" : "merbivore", "textContent" : "Merbivore" }
	,{ "value" : "merbivore_soft", "textContent" : "Merbivore Soft" }
	,{ "value" : "mono_industrial", "textContent" : "Mono Industrial" }
	,{ "value" : "monokai", "textContent" : "Monokai" }
	,{ "value" : "nord_dark", "textContent" : "Nord Dark" }
	,{ "value" : "one_dark", "textContent" : "One Dark" }
	,{ "value" : "pastel_on_dark", "textContent" : "Pastel On Dark" }
	,{ "value" : "solarized_dark", "textContent" : "Solarized Dark" }
	,{ "value" : "solarized_light", "textContent" : "Solarized Light" }
	,{ "value" : "sqlserver", "textContent" : "SQL Server" }
	,{ "value" : "terminal", "textContent" : "Terminal" }
	,{ "value" : "textmate", "textContent" : "TextMate" }
	,{ "value" : "tomorrow", "textContent" : "Tomorrow" }
	,{ "value" : "tomorrow_night", "textContent" : "Tomorrow Night" }
	,{ "value" : "tomorrow_night_blue", "textContent" : "Tomorrow Night Blue" }
	,{ "value" : "tomorrow_night_bright", "textContent" : "Tomorrow Night Bright" }
	,{ "value" : "tomorrow_night_eighties", "textContent" : "Tomorrow Night Eighties" }
	,{ "value" : "twilight", "textContent" : "Twilight" }
	,{ "value" : "vibrant_ink", "textContent" : "Vibrant Ink" }
	,{ "value" : "xcode", "textContent" : "Xcode" }
	];
const defaultTheme = "monokai";
const languages = [
	{ "value" : "abap", "textContent" : "ABAP" }
	,{ "value" : "abc", "textContent" : "ABC" }
	,{ "value" : "actionscript", "textContent" : "ActionScript" }
	,{ "value" : "ada", "textContent" : "ADA" }
	,{ "value" : "alda", "textContent" : "Alda" }
	,{ "value" : "apache conf", "textContent" : "Apache Conf" }
	,{ "value" : "apex", "textContent" : "Apex" }
	,{ "value" : "applescript", "textContent" : "Applescript" }
	,{ "value" : "aql", "textContent" : "AQL" }
	,{ "value" : "asciidoc", "textContent" : "AsciiDoc" }
	,{ "value" : "asl", "textContent" : "ASL" }
	,{ "value" : "assembly x86", "textContent" : "Assembly x86" }
	,{ "value" : "autohotkey", "textContent" : "AutoHotkey / AutoIt" }
	,{ "value" : "batchfile", "textContent" : "BatchFile" }
	,{ "value" : "behaviour", "textContent" : "Behaviour" }
	,{ "value" : "c9search", "textContent" : "C9Search" }
	,{ "value" : "c cpp", "textContent" : "C and C++" }
	,{ "value" : "cirru", "textContent" : "Cirru" }
	,{ "value" : "clojure", "textContent" : "Clojure" }
	,{ "value" : "cobol", "textContent" : "Cobol" }
	,{ "value" : "coffee", "textContent" : "CoffeeScript" }
	,{ "value" : "coldfusion", "textContent" : "ColdFusion" }
	,{ "value" : "crystal", "textContent" : "Crystal" }
	,{ "value" : "csharp", "textContent" : "C#" }
	,{ "value" : "csound document", "textContent" : "Csound Document" }
	,{ "value" : "csound orchestra", "textContent" : "Csound" }
	,{ "value" : "csound score", "textContent" : "Csound Score" }
	,{ "value" : "csp", "textContent" : "Csp" }
	,{ "value" : "css", "textContent" : "CSS" }
	,{ "value" : "curly", "textContent" : "Curly" }
	,{ "value" : "d", "textContent" : "D" }
	,{ "value" : "dart", "textContent" : "Dart" }
	,{ "value" : "diff", "textContent" : "Diff" }
	,{ "value" : "django", "textContent" : "Django" }
	,{ "value" : "dockerfile", "textContent" : "Dockerfile" }
	,{ "value" : "dot", "textContent" : "Dot" }
	,{ "value" : "drools", "textContent" : "Drools" }
	,{ "value" : "edifact", "textContent" : "Edifact" }
	,{ "value" : "eiffel", "textContent" : "Eiffel" }
	,{ "value" : "ejs", "textContent" : "EJS" }
	,{ "value" : "elixir", "textContent" : "Elixir" }
	,{ "value" : "elm", "textContent" : "Elm" }
	,{ "value" : "erlang", "textContent" : "Erlang" }
	,{ "value" : "forth", "textContent" : "Forth" }
	,{ "value" : "fortran", "textContent" : "Fortran" }
	,{ "value" : "fsharp", "textContent" : "FSharp" }
	,{ "value" : "fsl", "textContent" : "FSL" }
	,{ "value" : "ftl", "textContent" : "FreeMarker" }
	,{ "value" : "gcode", "textContent" : "Gcode" }
	,{ "value" : "gherkin", "textContent" : "Gherkin" }
	,{ "value" : "gitignore", "textContent" : "Gitignore" }
	,{ "value" : "glsl", "textContent" : "Glsl" }
	,{ "value" : "gobstones", "textContent" : "Gobstones" }
	,{ "value" : "golang", "textContent" : "Go" }
	,{ "value" : "graphqlschema", "textContent" : "GraphQLSchema" }
	,{ "value" : "groovy", "textContent" : "Groovy" }
	,{ "value" : "haml", "textContent" : "HAML" }
	,{ "value" : "handlebars", "textContent" : "Handlebars" }
	,{ "value" : "haskell", "textContent" : "Haskell" }
	,{ "value" : "haskell cabal", "textContent" : "Haskell Cabal" }
	,{ "value" : "haxe", "textContent" : "haXe" }
	,{ "value" : "hjson", "textContent" : "Hjson" }
	,{ "value" : "html", "textContent" : "HTML" }
	,{ "value" : "html elixir", "textContent" : "HTML Elixir" }
	,{ "value" : "html ruby", "textContent" : "HTML (Ruby)" }
	,{ "value" : "ini", "textContent" : "INI" }
	,{ "value" : "io", "textContent" : "Io" }
	,{ "value" : "jack", "textContent" : "Jack" }
	,{ "value" : "jade", "textContent" : "Jade" }
	,{ "value" : "java", "textContent" : "Java" }
	,{ "value" : "javascript", "textContent" : "JavaScript" }
	,{ "value" : "json", "textContent" : "JSON" }
	,{ "value" : "json5", "textContent" : "JSON5" }
	,{ "value" : "jsoniq", "textContent" : "JSONiq" }
	,{ "value" : "jsp", "textContent" : "JSP" }
	,{ "value" : "jssm", "textContent" : "JSSM" }
	,{ "value" : "jsx", "textContent" : "JSX" }
	,{ "value" : "julia", "textContent" : "Julia" }
	,{ "value" : "kotlin", "textContent" : "Kotlin" }
	,{ "value" : "latex", "textContent" : "LaTeX" }
	,{ "value" : "latte", "textContent" : "Latte" }
	,{ "value" : "less", "textContent" : "LESS" }
	,{ "value" : "liquid", "textContent" : "Liquid" }
	,{ "value" : "lisp", "textContent" : "Lisp" }
	,{ "value" : "livescript", "textContent" : "LiveScript" }
	,{ "value" : "logiql", "textContent" : "LogiQL" }
	,{ "value" : "logtalk", "textContent" : "Logtalk" }
	,{ "value" : "lsl", "textContent" : "LSL" }
	,{ "value" : "lua", "textContent" : "Lua" }
	,{ "value" : "luapage", "textContent" : "LuaPage" }
	,{ "value" : "lucene", "textContent" : "Lucene" }
	,{ "value" : "makefile", "textContent" : "Makefile" }
	,{ "value" : "markdown", "textContent" : "Markdown" }
	,{ "value" : "mask", "textContent" : "Mask" }
	,{ "value" : "matlab", "textContent" : "MATLAB" }
	,{ "value" : "maze", "textContent" : "Maze" }
	,{ "value" : "mediawiki", "textContent" : "MediaWiki" }
	,{ "value" : "mel", "textContent" : "MEL" }
	,{ "value" : "mips", "textContent" : "Mips" }
	,{ "value" : "mixal", "textContent" : "MIXAL" }
	,{ "value" : "mushcode", "textContent" : "MUSHCode" }
	,{ "value" : "mysql", "textContent" : "MySQL" }
	,{ "value" : "nginx", "textContent" : "Nginx" }
	,{ "value" : "nim", "textContent" : "Nim" }
	,{ "value" : "nix", "textContent" : "Nix" }
	,{ "value" : "nsis", "textContent" : "NSIS" }
	,{ "value" : "nunjucks", "textContent" : "Nunjucks" }
	,{ "value" : "objectivec", "textContent" : "Objective-C" }
	,{ "value" : "ocaml", "textContent" : "OCaml" }
	,{ "value" : "pascal", "textContent" : "Pascal" }
	,{ "value" : "perl", "textContent" : "Perl" }
	,{ "value" : "perl6", "textContent" : "Perl 6" }
	,{ "value" : "pgsql", "textContent" : "pgSQL" }
	,{ "value" : "php", "textContent" : "PHP" }
	,{ "value" : "php laravel blade", "textContent" : "PHP (Blade Template)" }
	,{ "value" : "pig", "textContent" : "Pig" }
	,{ "value" : "plain text", "textContent" : "Plain text" }
	,{ "value" : "powershell", "textContent" : "Powershell" }
	,{ "value" : "praat", "textContent" : "Praat" }
	,{ "value" : "prisma", "textContent" : "Prisma" }
	,{ "value" : "prolog", "textContent" : "Prolog" }
	,{ "value" : "properties", "textContent" : "Properties" }
	,{ "value" : "protobuf", "textContent" : "Protobuf" }
	,{ "value" : "puppet", "textContent" : "Puppet" }
	,{ "value" : "python", "textContent" : "Python" }
	,{ "value" : "qml", "textContent" : "QML" }
	,{ "value" : "r", "textContent" : "R" }
	,{ "value" : "raku", "textContent" : "Raku" }
	,{ "value" : "razor", "textContent" : "Razor" }
	,{ "value" : "rdoc", "textContent" : "RDoc" }
	,{ "value" : "red", "textContent" : "Red" }
	,{ "value" : "redshift", "textContent" : "Redshift" }
	,{ "value" : "rhtml", "textContent" : "RHTML" }
	,{ "value" : "rst", "textContent" : "RST" }
	,{ "value" : "ruby", "textContent" : "Ruby" }
	,{ "value" : "rust", "textContent" : "Rust" }
	,{ "value" : "sass", "textContent" : "SASS" }
	,{ "value" : "scad", "textContent" : "SCAD" }
	,{ "value" : "scala", "textContent" : "Scala" }
	,{ "value" : "scheme", "textContent" : "Scheme" }
	,{ "value" : "scss", "textContent" : "SCSS" }
	,{ "value" : "sh", "textContent" : "SH" }
	,{ "value" : "sjs", "textContent" : "SJS" }
	,{ "value" : "slim", "textContent" : "Slim" }
	,{ "value" : "smarty", "textContent" : "Smarty" }
	,{ "value" : "smithy", "textContent" : "Smithy" }
	,{ "value" : "snippets", "textContent" : "Snippets" }
	,{ "value" : "soy template", "textContent" : "Soy Template" }
	,{ "value" : "space", "textContent" : "Space" }
	,{ "value" : "sparql", "textContent" : "SparQL" }
	,{ "value" : "sql", "textContent" : "SQL" }
	,{ "value" : "sqlserver", "textContent" : "SQLServer" }
	,{ "value" : "stylus", "textContent" : "Stylus" }
	,{ "value" : "svg", "textContent" : "SVG" }
	,{ "value" : "swift", "textContent" : "Swift" }
	,{ "value" : "tcl", "textContent" : "Tcl" }
	,{ "value" : "terraform", "textContent" : "Terraform" }
	,{ "value" : "tex", "textContent" : "Tex" }
	,{ "value" : "text", "textContent" : "Text" }
	,{ "value" : "textile", "textContent" : "Textile" }
	,{ "value" : "toml", "textContent" : "Toml" }
	,{ "value" : "tsx", "textContent" : "TSX" }
	,{ "value" : "turtle", "textContent" : "Turtle" }
	,{ "value" : "twig", "textContent" : "Twig" }
	,{ "value" : "typescript", "textContent" : "Typescript" }
	,{ "value" : "vala", "textContent" : "Vala" }
	,{ "value" : "vbscript", "textContent" : "VBScript" }
	,{ "value" : "velocity", "textContent" : "Velocity" }
	,{ "value" : "verilog", "textContent" : "Verilog" }
	,{ "value" : "vhdl", "textContent" : "VHDL" }
	,{ "value" : "visualforce", "textContent" : "Visualforce" }
	,{ "value" : "wollok", "textContent" : "Wollok" }
	,{ "value" : "xml", "textContent" : "XML" }
	,{ "value" : "xquery", "textContent" : "XQuery" }
	,{ "value" : "yaml", "textContent" : "YAML" }
	,{ "value" : "zeek", "textContent" : "Zeek" }
];
const defaultLanguage = "javascript";