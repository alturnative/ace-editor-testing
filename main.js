function onLoad(){
	let themeValues = themes.map(function (x) {return x.value;});
	let themeTextContents = themes.map(function (x) {return x.textContent;});
	let languageValues = languages.map(function (x) {return x.value;});
	let languageTextContents = languages.map(function (x) {return x.textContent;});

	populateSelectFromArray(
		themeSelector
		, themeValues
		, themeTextContents
		, defaultTheme
	);
	populateSelectFromArray(
		languageSelector
		, languageValues
		, languageTextContents
		, defaultLanguage
	);
	
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/javascript");
	editor.setOptions({
		fontFamily: 'JetBrains Mono',
		fontSize: '16pt',
		enableBasicAutocompletion: true,
		enableLiveAutocompletion: true
	});
}

function populateSelectFromArray(selector, values, textContents, defaultValue) {
	if (typeof defaultValue === "undefined") {
		defaultValue = values[0];
	}
	
	for (let i = 0; i < values.length; i++) {
		let element = document.createElement("option");
		element.value = values[i];
		element.textContent = textContents[i];
		
		if (values[i] === defaultValue) {
			element.selected = "selected";
			element.textContent += " (Default)";
		}
		
		selector.appendChild(element);
	}
}

function themeChange(event){
	let theme = getSelectValueFromChangeEvent(event);
	//let editor = getEditor();
	editor.setTheme(themePrefix + theme);
}

function languageChange(event){
	let language = getSelectValueFromChangeEvent(event);
	//let editor = getEditor();
	editor.session.setMode(languagePrefix + language);
}

function getSelectValueFromChangeEvent(event) {
	return event.srcElement[event.srcElement.selectedIndex].value;
}

function getEditor() {
	return document.getElementById('editor').contentWindow.editor;
}